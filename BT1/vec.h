#ifndef _VEC_H
#define _VEC_H
typedef struct {
    double x, y;
} Vec;

double dot(Vec, Vec);
Vec sum(Vec, Vec);
Vec sub(Vec, Vec);
double len(Vec);
Vec mul(Vec, double);
Vec div(Vec, double);
void inc(Vec &, const Vec &);
void dec(Vec &, const Vec &);
#endif
