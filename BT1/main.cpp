#include "models.h"
#include "vec.h"
#include <math.h>
#include <stdlib.h>

void count_tree_types(const Farm &farm, int c[3]) {
    FOR(i, 0, 3) c[i] = 0;
    FOR(i, 0, farm.len) c[farm.tree[i].type]++;
}

double shortest_wall(const Farm &f) {
    double xmax, xmin, ymax, ymin;
    xmax = xmin = f.tree[0].x;
    ymax = ymin = f.tree[0].y;
    FOR(i, 0, f.len) {
        xmax = fmax(xmax, f.tree[i].x);
        ymax = fmax(ymax, f.tree[i].y);
        xmin = fmin(xmin, f.tree[i].x);
        ymin = fmin(ymin, f.tree[i].y);
    }
    printf("%lf %lf %lf %lf", xmax, ymax, xmin, ymin);
    return 2.0*(xmax - xmin + ymax - ymin);
}

Vec gradient(const Vec *a, int n, Vec x) {
    Vec tmp;
    Vec res = {0.0, 0.0};
    FOR(i,0,n) {
        tmp = sub(x, a[i]);
        inc(res, div(tmp, len(tmp)));
    }
    return res;
}
double cost(const Vec *a, int n, Vec x) {
    double res = 0.0;
    FOR(i, 0, n) res += len(sub(x, a[i]));
    return res;
}
double sqr(double x) { return x*x; }

// Gradient descent method
double pipe(const Farm &farm) {
    const int n = farm.len;
    const double precision = 1e-6;

    Vec *vec = (Vec*) malloc(n*sizeof(Vec));
    FOR(i, 0, n) vec[i] = {farm.tree[i].x, farm.tree[i].y};

    Vec x = {0.0, 0.0};
    FOR(i, 0, n) inc(x, vec[i]);
    x = div(x, (double)n);

    double gamma = 0.01;
    double f = cost(vec, n, x);
    Vec g = gradient(vec, n, x);
    double f0 = 1e9;
    Vec g0, x0;
    int count = 0;
    while (abs(f - f0) > precision) {
        if (count>0) {
            gamma = dot(sub(x, x0), sub(g, g0))/sqr(len(sub(g, g0)));
        }
        x0 = x;
        dec(x, mul(g, gamma));
        f0 = f;
        f = cost(vec, n, x);
        g0 = g;
        g = gradient(vec, n, x);
        count++;
    }
    return f;
}

int main() {
    FILE* f = fopen("NongTrai.in", "r");
    Farm farm = read_farm(f);
    fclose(f);

    int count[3];
    count_tree_types(farm, count);
    double wall = shortest_wall(farm);
    double pipe_cost = pipe(farm);

    f = fopen("NongTrai.out", "w");
    fprintf(f, "%d %d %d\n", count[0], count[1], count[2]);
    fprintf(f, "%lf\n", wall);
    fprintf(f, "%lf\n", pipe_cost);
    fclose(f);
    delete_farm(&farm);
    return 0;
}
