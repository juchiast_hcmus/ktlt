#ifndef _MODELS_H
#define _MODELS_H
#include <stdio.h>

#define FOR(i, a, b) for(int i=(a);i<(b);i++)

typedef struct {
    double x, y;
    int type;
} Tree;

typedef struct {
    Tree *tree;
    int len;
} Farm;

void read_tree(FILE* stream, Tree &t);
void alloc_farm(Farm &f, int len);
void delete_farm(Farm *f);
Farm read_farm(FILE* stream);
#endif
