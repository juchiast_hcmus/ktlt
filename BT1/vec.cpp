#include "vec.h"
#include <math.h>

double dot(Vec x, Vec y) {
    return x.x*y.x + x.y*y.y;
}
Vec sum(Vec x, Vec y) {
    return {x.x+y.x, x.y+y.y};
}
Vec sub(Vec x, Vec y) {
    return {x.x-y.x, x.y-y.y};
}
double len(Vec x) {
    return sqrt(x.x*x.x + x.y*x.y);
}
Vec mul(Vec x, double k) {
    return {x.x*k, x.y*k};
}
Vec div(Vec x, double k) {
    return {x.x/k, x.y/k};
}
void inc(Vec &x, const Vec &y) {
    x.x += y.x;
    x.y += y.y;
}
void dec(Vec &x, const Vec &y) {
    x.x -= y.x;
    x.y -= y.y;
}
