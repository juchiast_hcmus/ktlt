#include <stdlib.h>
#include <stdio.h>
#include "models.h"

void read_tree(FILE* stream, Tree &t) {
    fscanf(stream, "%lf%lf%d", &t.x, &t.y, &t.type);
}

void alloc_farm(Farm &f, int len) {
    f.len = len;
    f.tree = (Tree*) malloc(sizeof(Tree)*f.len);
}
void delete_farm(Farm *f) {
    free(f->tree);
}

Farm read_farm(FILE* stream) {
    Farm f;
    fscanf(stream, "%d", &f.len);
    alloc_farm(f, f.len);
    FOR(i, 0, f.len) read_tree(stream, f.tree[i]);
    return f;
}
