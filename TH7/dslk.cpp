#include "dslk.h"
#include <assert.h>

#define NEXT(p) p = p->next
#define CHECK(c) if (!(c)) return false;
#define ITER(p, pos, end) for (node *p=pos; p && p != end; p = p->next)

data cloneData(const data &src) {
    data res;
    res.size = src.size;
    res.raw = new uchar[res.size];
    memcpy(res.raw, src.raw, res.size);
    return res;
}

data newData(void *p, int size) {
    data res;
    res.raw = (uchar*)p;
    res.size = size;
    return res;
}

node *newNode(data x, node *next, bool clone) {
    node *res = new node;
    if (!res) return NULL;
    if (clone) x = cloneData(x);
    res->d = x;
    res->next = next;
    return res;
}
void deleteNode(node *node) {
    if (!node) return;
    delete[] node->d.raw;
    delete node;
}

dslk newEmptyList() { return {NULL, NULL}; }
bool addAfter(dslk &ds, node *pos, data x, bool clone) {
    assert(ds.head);
    assert(ds.tail);
    CHECK(pos);
    node *node = newNode(x, pos->next, clone);
    CHECK(node);
    pos->next = node;
    if (pos == ds.tail) ds.tail = node;
    return true;
}
bool addAt(dslk &ds, node *node, data x, bool clone) {
    CHECK(node);
    // For empty list
    if (!ds.head) {
        assert(!ds.tail);
        ds.head = ds.tail = newNode(x, NULL, clone);
        CHECK(ds.head);
        return true;
    }
    assert(ds.tail); // List is not empty, tail must not be NULL
    addAfter(ds, node, node->d, false);
    if (clone) x = cloneData(x);
    node->d = x;
    return true;
}
bool addTail(dslk &ds, data x, bool clone) {
    // For empty list
    if (!ds.head) {
        assert(!ds.tail);
        ds.head = ds.tail = newNode(x, NULL, clone);
        CHECK(ds.head);
        return true;
    }
    return addAfter(ds, ds.tail, x, clone);
}
bool addHead(dslk &ds, data x, bool clone) {
    return addAt(ds, ds.head, x, clone);
}

bool before_tail(const node *p, const node *tail) {
    CHECK(p);
    return p->next == tail;
}
bool removeAt(dslk &ds, node* pos) {
    CHECK(pos);
    // List that has 1 element
    if (pos == ds.head && ds.head == ds.tail) {
        ds.head = ds.tail = NULL;
        deleteNode(pos);
        return true;
    }
    if (pos == ds.tail) {
        ds.tail = find(ds.tail, before_tail, ds.head);
        ds.tail->next = NULL;
        deleteNode(pos);
        return true;
    }
    node *bk = pos->next;
    data old_data = pos->d;
    assert(bk);
    *pos = *(pos->next);
    if (bk == ds.tail) ds.tail = pos;
    delete bk;
    delete[] old_data.raw;
    return true;
}

bool destroy(dslk &ds) {
    for (node *p = ds.head; p;) {
        node *q = p->next;
        deleteNode(p);
        p = q;
    }
    ds.head = ds.tail = NULL;
    return true;
}

void count_fn(void *res, data &) { (*(int*)res)++; }
int count(const dslk &ds) {
    int res = 0;
    each(count_fn, &res, ds.head);
    return res;
}

void each(void (*f)(void *res, data &), void *res, node *pos, node *end) {
    ITER(p, pos, end) f(res, p->d);
}
void each(void (*f)(data &), node *pos, node *end) {
    ITER(p, pos, end) f(p->d);
}
void each(void (*f)(void *), node *pos, node *end) {
    ITER(p, pos, end) f(p->d.raw);
}
void each(void (*f)(void *res, node *), void *res, node *pos, node *end) {
    ITER(p, pos, end) f(res, p);
}
void each(void (*f)(node *), node *pos, node *end) {
    ITER(p, pos, end) f(p);
}

bool equal(const data &p, const data &x) {
    CHECK(p.size == x.size);
    for (int i=0; i<x.size; i++) {
        CHECK(p.raw[i] == x.raw[i]);
    }
    return true;
}
node *find(const data& sample, bool (*f)(const data&, const data &), node *pos, node *end, bool del) {
    ITER(p, pos, end) if (f(p->d, sample)) {
        if (del) delete[] sample.raw;
        return p;
    }
    if (del) delete[] sample.raw;
    return NULL;
}
node *find(const data& sample, bool (*f)(const node *, const data &), node *pos, node *end, bool del) {
    ITER(p, pos, end) if (f(p, sample)) {
        if (del) delete[] sample.raw;
        return p;
    }
    if (del) delete[] sample.raw;
    return NULL;
}
node *find(const node *sample, bool (*f)(const node *, const node *), node *pos, node *end) {
    ITER(p, pos, end) if (f(p, sample)) return p;
    return NULL;
}
