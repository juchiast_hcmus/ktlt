#include <iostream>
#include "dslk.h"
using namespace std;

inline data newInt(int x) {
    return newData(new int[1]{x}, sizeof(int));
}
void print_int(void *data) {
    cout << *(int*)data << " ";
}
void test_int() {
    dslk ds = newEmptyList();
    addTail(ds, newInt(1), false);
    addTail(ds, newInt(2), false);
    addHead(ds, newInt(0), false);
    addTail(ds, newInt(3), false);
    addAt(ds, find(newInt(2), equal, ds.head, NULL, true), newInt(-1), false);
    addAt(ds, ds.tail, newInt(100), false);
    cout << "tail: " << *(int*)ds.tail->d.raw << endl;
    each(print_int, ds.head);
    cout << endl << "count: " << count(ds) << endl;
    removeAt(ds, ds.head);
    removeAt(ds, find(newInt(100), equal, ds.head, NULL, true));
    removeAt(ds, ds.tail);
    each(print_int, ds.head);
    destroy(ds);
}

int main() {
    test_int();
    return 0;
}
