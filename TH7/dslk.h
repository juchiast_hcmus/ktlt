#ifndef DSLK_H
#define DSLK_H
#include <cstring>

typedef unsigned char uchar;

struct data {
    uchar *raw;
    int size;
};

struct node {
    data d;
    node *next;
};

struct dslk {
    node *head;
    node *tail;
};

data newData(void *, int size);
data clone(const data &);

node *newNode(data, node *next, bool clone=true);
void deleteNode(node *);

dslk newEmptyList();
bool addAfter(dslk &, node* pos, data, bool clone=true);
bool addAt(dslk &, node* pos, data, bool clone=true);
bool addTail(dslk &, data, bool clone=true);
bool addHead(dslk &, data, bool clone=true);

bool removeAt(dslk &, node* pos);
bool destroy(dslk &);

void each(void (*f)(void *res, data &), void *result, node *pos, node *end=NULL);
void each(void (*f)(data &), node *pos, node *end=NULL);
void each(void (*f)(void *), node *pos, node *end=NULL);
void each(void (*f)(void *res, node *), void *result, node *pos, node *end=NULL);
void each(void (*f)(node *), node *pos, node *end=NULL);

int count(const dslk &ds);

bool equal(const data &, const data &);
node *find(const data &sample, bool (*f)(const data &, const data &), node *pos, node *end=NULL, bool del=false);
node *find(const data &sample, bool (*f)(const node *, const data &), node *pos, node *end=NULL, bool del=false);
node *find(const node *sample, bool (*f)(const node *, const node *), node *pos, node *end=NULL);

#endif
