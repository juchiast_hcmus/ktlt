// Bai` ma~ di tuan`
//

#include <iostream>
using namespace std;
int di[] = {2, 2, 1, -1, -2, -2, -1, 1};
int dj[] = {1, -1, -2, -2, -1, 1, 2, 2};
int c[8][8];
int kq[64][2];
bool boo[8][8];

bool in_bound(int i, int j) {
    return i>=0 && i<8 && j>=0 && j<8;
}

void set(int i, int j, bool dec) {
    int ii, jj;
    for (int x=0; x<8; x++) {
        ii = i + di[x];
        jj = j + dj[x];
        if (in_bound(ii, jj)) {
            if (dec) c[ii][jj]--;
            else c[ii][jj]++;
        }
    }
}

int maxm = 0;

bool solve(int m) {
    if (m == 64) return true;
    if (c[kq[m-1][0]][kq[m-1][1]] <= 0) return false;

    for (int k=0; k<=8; k++) {
        for (int x=0; x<8; x++) {
            int i = kq[m-1][0] + di[x];
            int j = kq[m-1][1] + dj[x];
            if (!in_bound(i, j)) continue;
            if (boo[i][j] && c[i][j] == k) {
                kq[m][0] = i; kq[m][1] = j;
                boo[i][j] = false;
                set(i, j, true);
                if (solve(m+1)) return true;
                boo[i][j] = true;
                set(i, j, false);
            }
        }
    }
    return false;
}

int main() {
    for (int i=0; i<8; i++) for (int j=0; j<8; j++) {
        c[i][j] = 0;
        for (int x=0; x<8; x++) if (in_bound(i+di[x], j+dj[x])) c[i][j]++;
        boo[i][j] = true;
    }
    int i, j;
    cout << "Nhap o bat dau (dong va cot danh so tu 0): ";
    cin >> i >> j;
    if (!in_bound(i, j)) {
        cout << "Nhap sai\n";
        return 0;
    }
    kq[0][0] = i; kq[0][1] = j;
    boo[i][j] = false;
    set(i, j, true);
    if (solve(1)) {
        for (int i=0; i<64; i++) printf("(%d,%d)\n", kq[i][0], kq[i][1]);
    } else {
        printf("Khong co loi giai;\n");
    }
    return 0;
}
