// Bai thap Ha Noi
//

#include <iostream>

void chuyen(int n, int a, int c, int b) {
    if (n == 1) {
        printf("Lay %c bo qua %c\n", 'A'+a, 'A'+c);
    } else {
        chuyen(n-1, a, b, c);
        printf("Lay %c bo qua %c\n", 'A'+a, 'A'+c);
        chuyen(n-1, b, c, a);
    }
}

int main() {
    printf("Nhap so dia: ");
    int n;
    std::cin >> n;
    chuyen(n, 0, 2, 1);
    return 0;
}
