// Bai` tam' quan hau
//

#include <iostream>
using namespace std;

bool ok(int q[8]) {
    static int t[16];
    for (int i=0; i<16; i++) t[i] = 0;
    for (int i=0; i<8; i++) {
        if (t[q[i] + i] > 0) return false;
        t[q[i] + i] ++;
    }
    for (int i=0; i<16; i++) t[i] = 0;
    for (int i=0; i<8; i++) {
        if (t[q[i] - i + 7] > 0) return false;
        t[q[i] - i + 7] ++;
    }
    return true;
}

void print(int q[8]) {
    for (int i=0; i<8; i++) printf("(%d, %d), ", i+1, q[i]+1);
    printf("\n");
}

void permute(int q[8], int n) {
    if (n == 1) {
        if (ok(q)) print(q);
        return;
    }
    for (int i=0; i<n-1; i++) {
        permute(q, n-1);
        if (n % 2 != 0) swap(q[0], q[n-1]);
        else swap(q[i], q[n-1]);
    }
    permute(q, n-1);
}

int main() {
    int q[8] = {0, 1, 2, 3, 4, 5, 6, 7};
    permute(q, 8);
    return 0;
}
