#include <iostream>
using namespace std;

void cau1() {
    int a[] = {1, 4, 2, 2, 4, 2, 1, 4, 2};
    int n = 9;
    for (int i=0; i<n; i++) for (int j=i+1; j<n; j++) {
        if (a[i] < a[j]) swap(a[i], a[j]);
    }
    for (int i=0; i<n; i++) cout << a[i] << " ";
    cout << endl;
}
void cau2() {
    int a[] = {1, 4,2, 2, 4, 2, 1, 4, 2};
    int n = 9;
    for (int i=0; i<n; i++) for (int j=i+1; j<n; j++) {
        if (a[i]%2==0 && a[j]%2==0 && a[i] < a[j]) swap(a[i], a[j]);
    }
    for (int i=0; i<n; i++) cout << a[i] << " ";
    cout << endl;
}
void cau3() {
    int a[] = {1, 4,2, 2, 4, 2, 1, 4, 2};
    int n = 9;
    for (int i=0; i<n; i++) for (int j=i+1; j<n; j++) {
        if (a[i] < 0 && a[j] < 0 && a[i] < a[j]) swap(a[i], a[j]);
        if (a[i] > 0 && a[j] > 0 && a[i] > a[j]) swap(a[i], a[j]);
    }
    for (int i=0; i<n; i++) cout << a[i] << " ";
    cout << endl;
}
bool is_prime(int a) {
    for (int i = 2; i*i <= a; i++) if (a%i==0) return false;
    return true;
}
void cau4() {
    int a[] = {1, 4,2, 2, 4, 2, 1, 4, 2};
    int n = 9;
    for (int i=0; i<n; i++) for (int j=i+1; j<n; j++) {
        if (is_prime(a[i]) && is_prime(a[j]) && a[i] > a[j]) swap(a[i], a[j]);
    }
    for (int i=0; i<n; i++) cout << a[i] << " ";
    cout << endl;
}
void cau5() {
    int a[] = {1, 4,2, 2, 4, 2, 1, 4, 2};
    int n = 9;
    for (int i=0; i<n; i++) for (int j=i+1; j<n; j++) {
        if (a[i]%2==0 && a[j]%2!=0) swap(a[i], a[j]);
        else if (a[i]%2==0 && a[i]>a[j]) swap(a[i], a[j]);
        else if (a[i]%2!=0 && a[i]<a[j]) swap(a[i], a[j]);
    }
    for (int i=0; i<n; i++) cout << a[i] << " ";
    cout << endl;
}
void cau6() {
    double a[] = {1, 4,2, 2, 4, 2, 1, 4, 2};
    int n = 9;
    for (int i=0; i<n; i++) for (int j=i+1; j<n; j++) {
        if (a[i] > a[j]) swap(a[i], a[j]);
    }
    for (int i=0; i<n; i++) cout << a[i] << " ";
    cout << endl;
}

int main() {
    cau1();
    cau2();
    cau3();
    cau4();
    cau5();
    cau6();
    return 0;
}
