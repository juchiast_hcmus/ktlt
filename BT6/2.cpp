// Bai 6.2

#include <iostream>
using namespace std;

bool is_prime(int n) {
    if (n <=1) return false;
    for (int i=2; i*i <= n; i++) if (n%i == 0) return false;
    return true;
}
int sum_evens(int *a, int n) {
    return (n<1? 0:sum_evens(a, n-1)) + (1-a[n-1]%2)*a[n-1];
}

void list_primes(int *a, int n) {
    if (n <= 0) return;
    list_primes(a, n-1);
    if (is_prime(a[n-1])) cout << a[n-1] << ", ";
}

int find(double *a, int n, double x) {
    if (n <= 0) return -1;
    if (x == a[n-1]) return n-1;
    return find(a, n-1, x);
}

int sum_evens_2d(int **a, int m, int n) {
    if (m == 0) return 0;
    int s = 0;
    for (int i=0; i<n; i++) if (a[m-1][i]%2 == 0) s += a[m-1][i];
    return s + sum_evens_2d(a, m-1, n);
}

int main() {
    int n, m;
    cout << "Nhap so luong phan tu cua mang: ";
    cin >> n;
    int *a = new int[n];
    for(int i=0; i<n; i++) {
        cout << "Nhap A[" << i << "] (so nguyen): ";
        cin >> a[i];
    }
    cout << "Cau A: Tong cac so chan: " << sum_evens(a, n) << endl;
    cout << "Cau D:Trich cac so nguyen to: "; list_primes(a, n); cout << endl;
    delete[] a;
   
    cout << "Cau C" << endl;
    double x;
    cout << "Nhap X (so thuc): ";
    cin >> x;
    cout << "Nhap so luong phan tu cua mang: ";
    cin >> n;
    double *b = new double[n];
    for(int i=0; i<n; i++) {
        cout << "Nhap B[" << i << "] (so thuc): ";
        cin >> b[i];
    }
    cout << "Vi tri cua X (-1 nghia la khong co): " << find(b, n, x) << endl;
    delete[] b;

    int **c;
    cout << "Cau B" << endl;
    cout << "Nhap M, N (so nguyen): ";
    cin >> m >> n;
    c = new int*[m];
    for (int i=0; i<m; i++) c[i] = new int[n];

    for (int i=0; i<m; i++) for (int j=0; j<m; j++) {
        cout << "Nhap C[" << i << "][" << j << "] (so nguyen): ";
        cin >> c[i][j];
    }

    cout << "Tong cac so chan trong ma tran: " << sum_evens_2d(c, m, n) << endl;

    for (int i=0; i<m; i++) delete[] c[i];
    delete[] c;
    return 0;
}
