// Bai 3d
//

#include <iostream>
#include <string.h>
using namespace std;

void permutate(char *s, int n, int count[]) {
    if (n == 0) cout << s << endl;
    else {
        for (int i=0; i < 256; i++) if (count[i] > 0) {
            count[i]--;
            s[n-1] = (char)i;
            permutate(s, n-1, count);
            count[i]++;
        }
    }
}

int main() {
    cout << "Nhap chuoi: ";
    char s[50];
    cin >> s;
    int len = strlen(s);
    int count[256];
    for (int i=0; i<256; i++) count[i] = 0;
    for (int i=0; i<len; i++) count[(unsigned int)s[i]]++;
    cout << "Cac hoan vi cua s: " << endl;
    permutate(s, len, count);
    return 0;
}
