// Bai 3a
//

#include <iostream>
#include <string.h>
using namespace std;

void reverse(char *s, int i, int j) {
    if (i>=j) return;
    swap(s[i++], s[j--]);
    reverse(s, i, j);
}

int main() {
    char s[50];
    cout << "Nhap chuoi: ";
    cin >> s;
    reverse(s, 0, strlen(s)-1);
    cout << "Chuoi sau khi dao: " << s << endl;
    return 0;
}
