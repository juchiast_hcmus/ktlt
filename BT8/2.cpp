// Bai 8.2
//

#include <iostream>
using namespace std;

typedef int Data;
struct SList {
    Data data;
    SList *next;
};

int count(SList *head) {
    int result = 0;
    while (head) {
        result++;
        head = head->next;
    }
    return result;
}

void reverse(SList *&head) {
    int len = count(head);
    SList** tmp = new SList*[len];
    for (int i=0; i<len; i++) {
        tmp[i] = head;
        head = head->next;
    }
    head = tmp[len-1];
    for (int i=len-1; i>0; i--) {
        tmp[i]->next = tmp[i-1];
    }
    tmp[0]->next = NULL;
    delete[] tmp;
}

// Ascending order
void insert(SList *&head, int x) {
    SList* node = new SList;
    node->data = x;
    node->next = NULL;
    if (!head) {
        head = node;
        return;
    }
    if (head->data >= node->data) {
        node->next = head;
        head = node;
        return;
    }

    SList* p = head;
    SList* q = NULL;
    while (p && p->data < node->data) {
        q = p;
        p = p->next;
    }
    if (!q) {
        printf("Loi!!!\n");
        return;
    }
    node->next = q->next;
    q->next = node;
}

void print(SList *head) {
    while (head) {
        cout << head->data;
        if (head->next) cout << "->";
        head = head->next;
    }
    cout << endl;
}

int main() {
    SList *list = NULL;
    insert(list, 4);
    insert(list, 3);
    insert(list, 10);
    insert(list, 1);
    insert(list, 5);
    insert(list, 1);
    insert(list, 2);
    print(list);
    reverse(list);
    print(list);
    return 0;
}
