// Bai 8.1
//

#include <iostream>
using namespace std;

typedef int Data;
struct DList {
    Data data;
    DList *next;
    DList *prev;
};

bool isEmpty(const DList *head) { return head==NULL; }

DList* findI(DList *head, int i) {
    while (head && i-->0) head = head->next;
    return head;
} 
DList* findX(DList *head, Data x) {
    while(head && head->data!=x) head = head->next;
    return head;
}
DList* findRear(DList *head) {
    while (head && head->next) head = head->next;
    return head;
}

void addAtHead(DList *&head, Data x) {
    DList* tmp = new DList{x, head, NULL};
    if (head) head->prev = tmp;
    head = tmp;
}
void addAtRear(DList *&head, Data x) {
    DList *rear = findRear(head);
    if (!rear) addAtHead(head, x);
    else rear->next = new DList{x, NULL, rear};
}
void addAfterNode(DList *node, Data x) {
    if (!node) return;
    DList *tmp = new DList{x, node->next, node};
    node->next->prev = tmp;
    node->next = tmp;
}

void removeHead(DList *&head) {
    if (!head) return;
    DList* tmp = head;
    head = head->next;
    delete tmp;
    if (head) head->prev = NULL;
}

void removeRear(DList *&head) {
    DList* rear = findRear(head);
    if (!rear) return;
    if (rear->prev) rear->prev->next = NULL;
    else head = NULL; // Danh sach co 1 phan tu
    delete rear;
}

void removeNode(DList *&head, DList *node) {
    if (!node) return;
    if (node==head) removeHead(head);
    if (node->prev) node->prev->next = node->next;
    if (node->next) node->next->prev = node->prev;
    delete node;
}

void removeAll(DList *&head) {
    while(head) removeHead(head);
}

void printAll(DList *node) {
    if (!node) return;
    while (node && node->prev) node = node->prev;
    while (node) {
        cout << node->data;
        if (node->next) cout << "->";
        node = node->next;
    }
    cout << endl;
}

void init(DList *&head) {
    removeAll(head);
    head = NULL;
}

int main() {
    DList *head = NULL;
    addAtHead(head, 1);
    addAtHead(head, 3);
    addAtHead(head, 10);
    addAtRear(head, 5);
    addAfterNode(findX(head, 1), 0);
    addAfterNode(findI(head, 1), 6);
    printAll(head);
    removeHead(head);
    removeRear(head);
    removeNode(head, findX(head, 1));
    printAll(head);
    removeAll(head);
    return 0;
}
