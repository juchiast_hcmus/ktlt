#include "bmp.h"
#include <string.h>

u32* str_to_num(u32 *num, char* str) {
    *num = 0;
    usize len = strlen(str);
    loop(usize, 0, len, i) {
        if (str[i] < '0' || str[i] > '9') return NULL;
        *num = *num*10 + usize(str[i] - '0');
    }
    return num;
}

bool parse(int argc, char **argv, const char* regex, u32 *target) {
    loop(int, 2, argc-1, i) if (0 == strcmp(regex, argv[i])) {
        if (NULL != str_to_num(target, argv[i+1])) return true;
    }
    return false;
}

int main(int argc, char **argv) {
    char *file_path;
    u32 split_w, split_h;
    if (argc <= 1) {
        println("Cu phap: <ten file> [-h <so phan cat doc>] [-w <so phan cat ngang>]");
        return 0;
    } else {
        file_path = argv[1];
        if (!parse(argc, argv, "-w", &split_w)) split_w = 1;
        if (!parse(argc, argv, "-h", &split_h)) split_h = 1;
    }

    Image image = image_from_bmp_file(file_path);
    char path[300];
    loop_2d(u32, split_h, split_w, h, w) {
        Pos start = {w * image.width / split_w, h * image.height / split_h};
        Pos size = {image.width / split_w, image.height / split_h};
        if (w == split_w - 1) size.w = size.w + image.width % split_w;
        if (h == split_h - 1) size.h = size.h + image.height % split_h;
        if (0 > sprintf(path, "%s.%ux%u.bmp", file_path, w, h)) {
            panic("Unexpected error while generating file name.");
        }

        Image tmp = image_sub_image(image, start, size);
        image_to_bmp_file(path, tmp);
        image_free(tmp);
    }
    image_free(image);
    return 0;
}
