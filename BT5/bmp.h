#ifndef _BMP_H
#define _BMP_H
#include "rust.h"

typedef u32 Color;

struct Pos { u32 w, h; };

struct Image {
    u32 width, height;
    Color **colors;
};

Image image_from_bmp_file(const char *);
bool image_to_bmp_file(const char *, Image);
Image image_sub_image(Image, Pos, Pos);
void image_free(Image);

#endif
