#include "bmp.h"
#include <string.h>

Color** new_2d_array(u32 w, u32 h) {
    Color **r = new Color*[h];
    loop(usize, 0, h, i) r[i] = new Color[w];
    return r;
}

void read(FILE *f, usize pos, usize size, void *ptr) {
    fseek(f, pos, SEEK_SET);
    if (size != fread(ptr, 1, size, f)) {
        panic("Unexpected error while reading file, pos: %zu, size: %zu.", pos, size);
    }
}

usize get_file_size(FILE* f) {
    fseek(f, 0, SEEK_END);
    return ftell(f);
}

void image_free(Image img) {
    loop(usize, 0, img.height, i) delete[] img.colors[i];
    delete[] img.colors;
}

Image image_sub_image(Image img, Pos start, Pos size) {
    Image ret = {size.w, size.h, NULL};
    ret.colors = new_2d_array(ret.width, ret.height);
    loop_2d(usize, size.h, size.w, h, w) {
        ret.colors[h][w] = img.colors[h + start.h][w + start.w];
    }
    return ret;
}

// Panic if errors
Image image_from_bmp_file(const char* file_path) {
    FILE* f = fopen(file_path, "rb");
    if (f == NULL) panic("Cannot open %s for reading.", file_path);
    usize file_size = get_file_size(f);
    if (file_size < 0x26) {
        panic("File %s is invalid, file size is %zu, wanted as least %d.", file_path, file_size, 0x26);
    }
    //{{{ READ FILE METADATA
    u32 pixel_array_pos;
    u32 pixel_array_size;
    u16 color_deep;
    Pos image_size;
    read(f, 0xA, 4, &pixel_array_pos);
    read(f, 0x22, 4, &pixel_array_size);
    read(f, 0x12, 4, &image_size.w);
    read(f, 0x16, 4, &image_size.h);
    read(f, 0x1C, 2, &color_deep);
    //}}} END READ FILE METADATA
    
    if (color_deep != 24) panic("Sorry, only 24-bit BMPs are supported.");
    if (0 != pixel_array_size % image_size.h) panic("Invalid pixel_array_size.");

    // READ PIXEL ARRAY
    if (file_size < pixel_array_pos + pixel_array_size) {
        panic("File %s is invalid, file size is %zu, wanted %u.", file_path, file_size, pixel_array_pos + pixel_array_size);
    }
    u8 *pixel_arr = new u8[pixel_array_size];
    read(f, pixel_array_pos, pixel_array_size, pixel_arr);

    Color **colors = new_2d_array(image_size.w, image_size.h);
    usize color_size = color_deep / 8;
    // Size of a row (int bytes) in pixel_array
    usize row_size = pixel_array_size / image_size.h; 
    loop_2d (usize, image_size.w, image_size.h, w, h) {
        usize pos = (image_size.h - h - 1) * row_size + w * color_size;
        memcpy(&colors[h][w], &pixel_arr[pos], color_size);
    }

    delete[] pixel_arr;
    fclose(f);
    return {image_size.w, image_size.h, colors};
}

// Return true if success
bool image_to_bmp_file(const char *file_path, Image image) {
    const u64 ZERO = 0;
    const u32 COLOR_SIZE = 3;

    FILE *f = fopen(file_path, "wb");
    if (f == NULL) panic("Cannot open %s for writing", file_path);

    //{{{ Calculate BMP header
    const u16 BM = 0x4D42; // 'BM' - begin of BMP file
    u32 pixel_array_size;
    { // Calculate pixel_array_size
        u32 row_size = image.width * COLOR_SIZE;
        row_size = row_size + (4 - row_size % 4) % 4;
        pixel_array_size = row_size * image.height;
    }
    u32 pixel_array_pos = 0x36;
    u32 file_size = 0x36 + pixel_array_size;
    u32 header[] = {
        file_size, // file size in disk
        0x0, // Dedicated part (unused part)
        pixel_array_pos, // pixel array pos
        40, // DIB size
        image.width,
        image.height,
        (24 << 16) + 1, // Number of color layers (=1), and color deep (=24)
        0x0, // Compression method = 0 (No compression)
        pixel_array_size, // pixel array size (in bytes)
        0x0, // Pixel per meters? (width)
        0x0, // Pixel per meters? (height)
        // We use BMP 24-bit => no colors table
        0x0, // Number of colors in colors table
        0x0, // Number of main colors in colors table
    };
    //}}} End Calulate BMP header

// Try writing to file, close the file and return false if failed
#define wtry(w) {\
    w;\
    if (ferror(f)) {\
        fclose(f);\
        println("Error while writing %s", file_path);\
        return false;\
    }\
}
    // Write header
    wtry(fwrite(&BM, 2, 1, f));
    wtry(fwrite(header, sizeof(header), 1, f)); // XXX May fail if sizeof(header) is not its intended size
    // Write pixel_array
    usize padding = (4 - image.width * 3 % 4)% 4;
    loop_2d(usize, image.height, image.width, h, w) {
        wtry(fwrite(&image.colors[image.height - h - 1][w], 1, COLOR_SIZE, f));
        if (w == image.width - 1) wtry(fwrite(&ZERO, 1, padding, f));
    }
    fclose(f);
#undef wtry
    return true;
}
