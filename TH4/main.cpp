#include "rust.h"
#include <iostream>
using namespace std;

i64 s_1(i64 n) { return n <= 1 ? 1 : n * s_1(n - 1); }
f64 s_2(i64 n) { return n <= 0 ? 1 : s_2(n - 1) + 1. / f64(2 * n + 1); }
f64 s_3(i64 n) { return n <= 0 ? 0.5 : s_3(n - 1) + f64(2 * n + 1) / f64(2 * n + 2); }
f64 s_4(i64 n, f64 x) { return n <= 1 ? x : x * (1 + s_4(n - 1, x)); }
f64 s_5(i64 n, f64 x)
{
    static f64 t, m;
    if (n <= 1) {
        t = x;
        m = 1;
        return x;
    } else {
        f64 last = s_5(n - 1, x);
        t *= x;
        m += f64(n);
        return last + t / m;
    }
}

bool is_prime(i64 n)
{
    if (n <= 1)
        return false;
    for (i64 i = 2; i * i <= n; i++)
        if (n % i == 0)
            return false;
    return true;
}

i64 count_primes(i64* a, i64 n)
{
    return (n == 1 ? 0 : count_primes(a, n - 1)) + (is_prime(a[n - 1]) ? 1 : 0);
}

bool is_perfect(i64 n)
{
    i64 s = 0;
    for (i64 i = 1; i < n; i++)
        if (n % i == 0)
            s += i;
    return n == s;
}
i64 count_perfects(i64* a, i64 n)
{
    return (n == 1 ? 0 : count_perfects(a, n - 1)) + (is_perfect(a[n - 1]) ? 1 : 0);
}

i64 sum_evens(i64* a, i64 n)
{
    return (n < 1 ? 0 : sum_evens(a, n - 1)) + (1 - a[n - 1] % 2) * a[n - 1];
}

void list_primes(i64* a, i64 n)
{
    if (n <= 0)
        return;
    list_primes(a, n - 1);
    if (is_prime(a[n - 1]))
        cout << n - 1 << ", ";
}

void list_evens(i64* a, i64 n)
{
    if (n <= 0)
        return;
    list_evens(a, n - 1);
    if (a[n - 1] % 2 == 0)
        cout << n - 1 << ", ";
}

f64 sum(f64* a, i64 n)
{
    if (n <= 1)
        return 0.;
    return (a[n - 1] > a[n - 2] ? a[n - 1] : 0.) + sum(a, n - 1);
}

void qsort(i64* a, i64 l, i64 r)
{
    i64 m = a[(l + r) / 2];
    i64 i = l;
    i64 j = r;
    while (i < j) {
        while (a[i] < m)
            i++;
        while (a[j] > m)
            j--;
        if (i <= j)
            swap(a[i++], a[j--]);
    }
    if (l < j)
        qsort(a, l, j);
    if (i < r)
        qsort(a, i, r);
}

i64 count_values(i64* a, i64 n)
{
    if (n <= 1)
        return 1;
    return (a[n - 1] != a[n - 2] ? 1 : 0) + count_values(a, n - 1);
}

int main()
{
    i64 n;
    f64 x;
    cout << "Nhap N (so nguyen): ";
    cin >> n;
    cout << "Nhap X (so thuc): ";
    cin >> x;

    cout << "S(n) = 1x2x...xN = " << s_1(n) << endl;
    cout << "S(n) = 1 + 1/3 +...+ 1/(2n+1) = " << s_2(n) << endl;
    cout << "S(n) = 1/2 + 3/4 +...+(2n+1)/(2n+2) = " << s_3(n) << endl;
    cout << "S(n,x) = x + x^2 +...+ x^n = " << s_4(n, x) << endl;
    cout << "S(n,x) = x + x^2/(1+2) +...+(x^n)/(1+2+...+n) = " << s_5(n, x) << endl;

    cout << "Nhap so luong phan tu cua mang: ";
    cin >> n;
    i64* a = new i64[n];
    loop(i64, 0, n, i)
    {
        cout << "Nhap a[" << i << "] (so nguyen): ";
        cin >> a[i];
    }
    cout << "So luong so nguyen to: " << count_primes(a, n) << endl;
    cout << "So luong so hoan thien: " << count_perfects(a, n) << endl;
    cout << "Tong cac so chan: " << sum_evens(a, n) << endl;
    cout << "Liet ke vi tri nguyen to: ";
    list_primes(a, n);
    cout << endl;
    cout << "Liet ke vi tri so chan: ";
    list_evens(a, n);
    cout << endl;
    qsort(a, 0, n - 1);
    cout << "So cac gia tri phan biet: " << count_values(a, n) << endl;
    delete[] a;

    cout << "Nhap so luong phan tu cua mang: ";
    cin >> n;
    f64* b = new f64[n];
    loop(i64, 0, n, i)
    {
        cout << "Nhap b[" << i << "] (so thuc): ";
        cin >> b[i];
    }
    cout << "Tong cac gia tri lon hon gia tri lien truoc: " << sum(b, n) << endl;
    delete[] b;
    return 0;
}
