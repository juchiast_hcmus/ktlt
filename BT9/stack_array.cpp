#include <stdio.h>

struct stack {
    int *data, top, size;
};

bool stack_init(stack& s, int size) {
    s.data = new int[size];
    s.top = -1;
    s.size = size;
    if (s.data == NULL) return false;
    return true;
}

bool stack_empty(const stack& s) {
    return s.top < 0;
}

bool stack_full(const stack& s) {
    return s.top >= s.size-1;
}

bool push(stack& s, int x) {
    if (stack_full(s)) return false;
    s.data[++s.top] = x;
    return true;
}

bool peek(const stack& s, int &res) {
    if (stack_empty(s)) return false;
    res = s.data[s.top];
    return true;
}

bool pop(stack& s) {
    if (stack_empty(s)) return false;
    s.top -= 1;
    return true;
}

int main() { return 0; }
