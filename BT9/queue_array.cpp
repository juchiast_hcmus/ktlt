#include <stdio.h>

struct queue {
    int *data, size;
    int in, out;
    int count;
};

bool init(queue& q, int size) {
    q.data = new int[size];
    if (q.data == NULL) return false;
    q.in = q.out = q.count = 0;
    return true;
}

bool is_empty(const queue& q) {
    return q.count == 0;
}

bool is_full(const queue& q) {
    return q.count == q.size;
}

bool peek(const queue& q, int &res) {
    if (is_empty(q)) return false;
    res = q.out;
    return true;
}

bool pop(queue& q) {
    if (is_empty(q)) return false;
    q.count--;
    q.out++;
    if (q.out == q.size) q.out = 0;
    return true;
}

bool push(queue& q, int x) {
    if (is_full(q)) return false;
    q.data[q.in] = x;
    q.in++;
    if (q.in == q.size) q.in = 0;
    q.count++;
    return true;
}

int main() { return 0; }
