#include <stdio.h>

struct SList { int data; SList* next; };
struct queue { SList *in, *out; };

bool init(queue& q) {
    q.in = q.out = NULL;
    return true;
}

bool is_empty(const queue& q) {
    return q.in == NULL;
}

bool is_full(const queue&) {
    return false;
}

bool peek(const queue& q, int &res) {
    if (is_empty(q)) return false;
    res = q.out->data;
    return true;
}

bool pop(queue& q) {
    if (is_empty(q)) return false;
    SList* tmp = q.out;
    q.out = q.out->next;
    delete tmp;
    if (q.out == NULL) q.in = NULL;
    return true;
}

bool push(queue& q, int x) {
    if (is_full(q)) return false;
    SList* in = new SList{x, q.in};
    if (in == NULL) return false;
    q.in = in;
    if (q.out == NULL) q.out = q.in;
    return true;
}

int main() { return 0; }
