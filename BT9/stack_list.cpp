#include <stdio.h>

struct SList {
    int data;
    SList *next;
};

struct stack {
    SList* top;
};

void stack_init(stack& s) {
    s.top = NULL;
}

bool stack_full(const stack&) {
    return false;
}

bool stack_empty(const stack& s) {
    return s.top == NULL;
}

bool peek(const stack& s, int &res) {
    if (stack_empty(s)) return false;
    res = s.top->data;
    return true;
}

bool pop(stack& s) {
    if (stack_empty(s)) return false;
    SList* tmp = s.top;
    s.top = s.top->next;
    delete tmp;
    return true;
}

bool push(stack& s, int x) {
    SList* top = new SList{x, s.top};
    if (top == NULL) return false;
    s.top = top;
    return true; 
}

int main() { return 0; }
