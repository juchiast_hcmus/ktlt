#ifndef _PHAN_SO_H
#define _PHAN_SO_H
#include <stdio.h>

typedef struct {
    int tu, mau;
} PhanSo;

void nhap_phan_so(FILE *, PhanSo *);
void xuat_phan_so(FILE *, const PhanSo *);
bool phan_so_am(const PhanSo *);

#endif
