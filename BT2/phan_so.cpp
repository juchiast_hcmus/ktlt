#include "phan_so.h"
#include <stdint.h>

void nhap_phan_so(FILE *f, PhanSo *x) {
    if (x == NULL) return;
    do {
        fscanf(f, "%d%d", &x->tu, &x->mau);
        if (x->mau == 0) printf("Mau phai khac 0, nhap lai.\n");
    } while (x->mau == 0);
}
void xuat_phan_so(FILE *f, const PhanSo *x) {
    if (x == NULL) return;
    fprintf(f, "%d/%d\n", x->tu, x->mau);
}
bool phan_so_am(const PhanSo *x) {
    return x!=NULL && x->mau!=0 && (int64_t)x->mau * (int64_t)x->tu < 0;
}
