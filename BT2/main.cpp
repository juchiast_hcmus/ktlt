#include <malloc.h>
#include <stdio.h>
#include <stdint.h>
#include "phan_so.h"

int main() {
    int n, i;
    PhanSo *mang;
    printf("Nhap so phan tu cua mang: ");
    scanf("%d", &n);
    mang = (PhanSo *) malloc(sizeof(PhanSo)*n);
    int m = 0;
    for (i=0; i<n; i++) {
        printf("Nhap phan so %d: ", i);
        nhap_phan_so(stdin, mang+i);
        if (phan_so_am(mang+i)) m++;
    }
    PhanSo *mang_am = (PhanSo *) malloc(sizeof(PhanSo)*m);
    m = 0;
    for (i=0; i<n; i++) if (phan_so_am(mang+i)) *(mang_am+m++) = *(mang+i);
    printf("Ket qua:\n");
    for (i=0; i<m; i++) xuat_phan_so(stdout, mang_am+i);
    free(mang);
    free(mang_am);
}
