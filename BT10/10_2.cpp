#include <iostream>
using namespace std;

void trace(int *a, int *f, int n) {
    int res = 0;
    for (int i=0; i<n; i++) res = max(res, f[i]);
    int *kq = new int[res];
    for (int i=res-1, j = n-1; i>=0; i--) {
        while (f[j] != i+1 && (j != n-1 || a[j] >= kq[i+1])) j--;
        kq[i] = a[j];
        j--;
    }

    cout << "Day con tang dai nhat:" << endl;
    for (int i=0; i<res; i++) {
        cout << kq[i] << " ";
    }
    cout << endl;

    delete[] kq;
}

int dq(int *a, int i, int *f) {
    if (i > 0) dq(a, i-1, f);
    f[i] = 0;
    for (int j=0; j<i; j++) if (a[j] < a[i]) f[i] = max(f[i], f[j]);
    f[i]++;
    return f[i];
}

void top_down(int *a, int n) {
    int *f = new int[n];
    for (int i=0; i<n; i++) f[i] = 0;
    dq(a, n-1, f);
    trace(a, f, n);
    delete[] f;
}

void bottom_up(int *a, int n) {
    int *f = new int[n];
    for (int i=0; i<n; i++) {
        f[i] = 0;
        for (int j=0; j<i; j++) if (a[j] < a[i]) f[i] = max(f[i], f[j]);
        f[i]++;
    }

    trace(a, f, n);

    delete[] f;
}

int main() {
    cout << "Nhap so phan tu cua day: ";
    int n;
    cin >> n;
    int *a = new int[n];
    for (int i=0; i<n; i++) {
        cout << "Nhap phan tu thu " << i << ": ";
        cin >> a[i];
    }

    bottom_up(a, n);
    top_down(a, n);

    delete[] a;
}
