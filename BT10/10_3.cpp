#include <iostream>
using namespace std;
int f[101][10001];
const int M = 10001;
const int oo = 1000000;

void trace(int *a, int n, int w) {
    if (f[n][w] == oo) cout << "Khong doi duoc";
    else while(w > 0) {
        if (w - a[n-1] < 0) n--;
        else if (f[n-1][w] < f[n-1][w-a[n-1]]) n--;
        else {
            cout << a[n-1] << " ";
            w -= a[n-1];
            n--;
        }
    }
    cout << endl;
}

void bottom_up(int *a, int n, int w) {
    for (int i=0; i<=n; i++) for (int j=0; j<=w; j++) f[i][j] = oo;
    f[0][0] = 0;
    for (int i=1; i<=n; i++) for (int j=0; j<=w; j++) {
        if (j - a[i-1] < 0) f[i][j] = f[i-1][j];
        else f[i][j] = min(f[i-1][j], f[i-1][j-a[i-1]] + 1);
    }
    trace(a, n, w);
}

int dq(int *a, int n, int w) {
    if (n == 0) return f[0][w];
    if (f[n][w] != oo) return f[n][w];
    if (w - a[n-1] < 0) f[n][w] = dq(a, n-1, w);
    else f[n][w] = min(dq(a, n-1, w), dq(a, n-1, w - a[n-1]));
    return f[n][w];
}
void top_down(int *a, int n, int w) {
    for (int i=0; i<=n; i++) for (int j=0; j<=w; j++) f[i][j] = oo;
    f[0][0] = 0;
    dq(a, n, w);
    trace(a, n, w);
}

int main() {
    cout << "Nhap so to tien: ";
    int n;
    cin >> n;
    cout << "Nhap so tien can doi: ";
    int w;
    cin >> w;
    int *a = new int[n];
    for (int i=0; i<n; i++) {
        cout << "Nhap to tien thu " << i << ": ";
        cin >> a[i];
    }
    bottom_up(a, n, w);
    top_down(a, n, w);
    delete[] a;
    return 0;
}
