#include <iostream>
using namespace std;
int a[1000][1000], f[1000][1000];
const int oo = 1000000;

int max3(int a, int b, int c) {
    return max(a, max(b, c));
}

void trace(int m, int n) {
    int sum = -1;
    for (int j=0; j<=n; j++) sum = max(sum, f[m][j]);
    cout << "So tien: " << sum << endl;
    int l = 1, r = n;
    for (int i = m; i>0; i--) {
        for (int j=l; j<=r; j++) if (sum == f[i][j]) {
            cout << "Tang " << i << " phong " << j << endl;
            l = j-1; r = j+1;
            sum -= a[i][j];
            break;
        }
    }
    cout << endl;
}

void bottom_up(int m, int n) {
    for (int i=0; i<1000; i++) for (int j=0; j<1000; j++) {
        f[i][j] = -oo;
    }
    for (int i=0; i<1000; i++) f[0][i] = 0;
    for (int i=1; i<=m; i++) for (int j=1; j<=n; j++) {
        f[i][j] = a[i][j] + max3(f[i-1][j], f[i-1][j-1], f[i-1][j+1]);
    }
    trace(m, n);
}

int dq(int i, int j, int m, int n) {
    if (i<1 || j<=1 || j>n) return f[i][j];
    dq(i-1, j-1, m, n);
    dq(i-1, j, m, n);
    dq(i-1, j+1, m, n);
    f[i][j] = a[i][j] + max3(f[i-1][j], f[i-1][j-1], f[i-1][j+1]);
    return f[i][j];
}

void top_down(int m, int n) {
    for (int i=0; i<1000; i++) for (int j=0; j<1000; j++) {
        f[i][j] = -oo;
    }
    for (int i=0; i<1000; i++) f[0][i] = 0;
    dq(m, n, m, n);
    trace(m, n);
}

int main() {
    int m, n;
    cin >> m >> n;
    for (int i=1; i<=m; i++) for (int j=1; j<=n; j++) cin >> a[i][j];
    bottom_up(m, n);
    top_down(m, n);
    return 0;
}
