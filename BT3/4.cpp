#include <stdio.h>

int **read_array(int &m, int &n) {
    scanf("%d%d", &m, &n);
    int **a = new int*[m];
    for (int i=0; i<m; i++) {
        a[i] = new int[n];
        for (int j=0; j<n; j++) scanf("%d", a[i]+j);
    }
    return a;
}

void delete_array(int **a, int m) {
    for (int i=0; i<m; i++) delete[] a[i];
    delete[] a;
}

bool is_prime(int x) {
    if (x < 2) return false;
    for (int i=2; i*i<=x; i++) if (x%i == 0) return false;
    return true;
}

int main() {
    int m, n;
    int **a = read_array(m, n);
    int count = 0;
    for (int i=0; i<m; i++) for (int j=0; j<n; j++) {
        if (is_prime(a[i][j])) count ++;
    }
    int *c = new int[count];
    count = 0;
    for (int i=0; i<m; i++) for (int j=0; j<n; j++) {
        if (is_prime(a[i][j])) c[count++] = a[i][j];
    }
    for (int i=0; i<count; i++) printf("%d ", c[i]);
    delete[] c;
    delete_array(a, m);
}
