#include <stdio.h>
#include <math.h>

void sort(int *a, int len, bool(*less)(int, int)) {
    for (int j=0; j<len; j++) {
        for (int i=1; i<len; i++) if (!less(a[i-1], a[i])) {
            int t = a[i];
            a[i] = a[i-1];
            a[i-1] = t;
        }
    }
}
void print(int *a, int n) {
    for (int i=0; i<n; i++) printf("%d ", a[i]);
    printf("\n");
}

bool cmp1(int a, int b) { return a < b; }
bool cmp2(int a, int b) { return a > b; }
bool cmp3(int a, int b) { return sin((double)a) < sin((double)b);  }

int main() {
    int n;
    scanf("%d", &n);
    int *a = new int[n];
    for (int i = 0; i < n; i++) scanf("%d", a + i);
    sort(a, n, cmp1);
    print(a, n);
    sort(a, n, cmp2);
    print(a, n);
    sort(a, n, cmp3);
    print(a, n);
    delete[]a;
    return 0;
}
