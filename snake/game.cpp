#include "game.h"
#include <ncurses.h>
#include <unistd.h>

void draw(Game &g) {
    static char mssv[] = "1612847";
    static char temp[1000];
    clear();
    loop(usize, 0, g.snake.size(), i) {
        sprintf(temp, "%c", mssv[i]);
        mvprintw(g.snake[i].y, g.snake[i].x, temp);
    }
    refresh();
}


Vec2 operator +(Vec2 a, Vec2 b) { return {a.x+b.x, a.y+b.y}; }
Vec2 operator -(Vec2 a, Vec2 b) { return {a.x-b.x, a.y-b.y}; }
void update(Game &g) {
    g.snake.pop_back();
    g.snake.push_front(g.snake.front() + g.snake.front() - g.snake[1]);
}

void run(Game &g) {
    initscr();
    while (g.state.type != DIED) {
        update(g);
        draw(g);
        sleep(1);
    }
    endwin();
}

Game new_game() {
    Game g;
    g.state.type = NORMAL;
    loop(i32, 0, 6, i) g.snake.push_front({i, 0});
    return g;
}
