#ifndef _RUST_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define usize size_t

#define i64 int64_t
#define i32 int32_t
#define i16 int16_t
#define i8 int8_t

#define u64 uint64_t
#define u32 uint32_t
#define u16 uint16_t
#define u8 uint8_t

#define f64 double
#define f32 float

#define loop(t, a, b, i) for(t i=(a); i<(b);(i)++)
#define loop_2d(t, a, b, x, y) loop(t, 0, a, x) loop(t, 0, b, y)
#define range(t, a, b, r, i) for (t i=(a);((a)<(b)?(i<=(b)):(i>=(b)));i+=((a)<(b)?(r):(-r)))

#define maximize(t, x, y) {t z = y;if((x)<z){(x)=z;}}
#define minimize(t, x, y) {t z = y;if((x)>z){(x)=z;}}

#define println(...) {printf(__VA_ARGS__); printf("\n");}
#define panic(...) {println(__VA_ARGS__); abort();}

#endif
