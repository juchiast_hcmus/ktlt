#ifndef _GAME_H
#define _GAME_H
#include "rust.h"
#include <deque>
using namespace std;

#define FOODS_COUNT 4

enum StateType {
    NORMAL,
    DYING,
    DIED,
    HOLE,
    NEXT_LEVEL,
    EAT_FOOD,
    TURN,
};

struct State {
    StateType type;
};

struct Vec2 {
    i32 x, y;
};

struct Game{
    Vec2 screen_size;
    State state;
    deque<Vec2> snake;
    Vec2 foods[FOODS_COUNT];
};

Game new_game();
void run(Game &);

#endif
