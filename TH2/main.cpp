#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

struct PS { int tu; int mau; };
istream& operator >>(istream &s, PS &p) {
    return s >> p.tu >> p.mau;
}
bool operator <(const PS &x, const PS &y) {
    return (double)x.tu / (double)x.mau < (double)y.tu / (double)y.mau;
}

#define FAIL(r) { cout << "Khong mo duoc file!!!!!!!" << endl; return r; }

#define IO_CHECK(in, out, r) if (in.fail() || out.fail()) \
{ cout << "Co loi xay ra!!!!!!!" << endl; return r; }

bool replace_chars(const string &source_name, const string &target_name, char original, char new_char) {
    ifstream in(source_name);
    if (!in) FAIL(false)
    string str;
    char ch;
    while (in.get(ch)) {
        str.push_back(ch==original? new_char : ch);
        IO_CHECK(in, in, false)
    }
    ofstream out(target_name);
    if (!out) FAIL(false)
    out << str;
    IO_CHECK(out, out, false)
    return true;
}

bool copy_file(const string &source_name, const string &target_name) {
    ifstream in(source_name, ios::binary);
    if (!in) FAIL(false)
    ofstream out(target_name, ios::binary);
    if (!out) FAIL(false)
    in.seekg(0, ios::end);
    ifstream::pos_type size = in.tellg();
    IO_CHECK(in, out, false)
    cout << size;
    char *buffer = new char[size];
    in.seekg(in.beg);
    in.read(buffer, size);
    out.write(buffer, size);
    delete[] buffer;
    IO_CHECK(in, out, false)
    return true;
}

bool read_array(const string &file_name, vector<PS> &ret) {
    ifstream in(file_name);
    if (!in) FAIL(false)
    PS tmp;
    while (in >> tmp) {
        ret.push_back(tmp);
        IO_CHECK(in, in, false)
    }
    return true;
}
bool write_array(const string &file_name, const vector<PS>& arr) {
    ofstream out(file_name);
    if (!out) FAIL(false)
    for (int i = 0; i < (int)arr.size(); i++) {
        out << arr[i].tu << "/" << arr[i].mau << endl;
        IO_CHECK(out, out, false)
    }
    return true;
}
bool count_chars(const string &source_name, const string &target_name) {
    ifstream in(source_name);
    if (!in) FAIL(false)
    ofstream out(target_name);
    if (!out) FAIL(false)
    int count[256];
    for (int i = 0; i < 256; i++) count[i] = 0;
    char ch;
    while (in.get(ch)) {
        IO_CHECK(in, in, false);
        count[(int)ch]++;
    }
    for (int i = 0; i < 256; i++) if (count[i] > 0) {
        out << (char)i << ":" << count[i] << endl;
        IO_CHECK(out, out, false);
    }
    return true;
}

void read_files_name(string &in_name, string &out_name) {
    cout << "Nhap ten file goc: ";
    getline(cin, in_name);
    cout << "Nhap ten file dich: ";
    getline(cin, out_name);
}

void task_1() {
    string in_name, out_name;
    read_files_name(in_name, out_name);

    char original, new_char;
    string tmp;
    cout << "Nhap ki tu goc: ";
    getline(cin, tmp);
    original = tmp[0];
    cout << "Nhap ki tu dich: ";
    getline(cin, tmp);
    new_char = tmp[0];
    if (replace_chars(in_name, out_name, original, new_char)) {
        cout << "Da ghi ra file dich!!" << endl;
    }
}

void task_2() {
    string in_name, out_name;
    read_files_name(in_name, out_name);
    if (copy_file(in_name, out_name)) {
        cout << "Da copy thanh cong!!" << endl;
    }
}

void task_3() {
    string in_name, out_name;
    read_files_name(in_name, out_name);
    vector<PS> arr;
    if (read_array(in_name, arr)) {;
        sort(arr.begin(), arr.end());
        if (write_array(out_name, arr)) {
            cout << "Da ghi ra file!!" << endl;
        }
    }
}

void task_4() {
    string in_name, out_name;
    read_files_name(in_name, out_name);
    if (count_chars(in_name, out_name)) {
        cout << "Da ghi ra file!!" << endl;
    }
}

int main() {
    void (*task[4])(void) = { task_1, task_2, task_3, task_4 };
    for (int i = 0; i < 4; i++) {
        cout << "Cau " << i + 1 << ": " << endl;
        task[i]();
    }
    return 0;
}
